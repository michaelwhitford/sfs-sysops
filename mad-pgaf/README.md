# Monitoring for SFS' infrastructure

This is a MAD PGaf, like from [the class](https://gitlab.com/sofreeus/mad-pgaf).

If this git repo is out of date, update it from the class, but be careful not to blow away the configs.

The running copy needs access to \*.int.sofree.us / 192.168.73.\* so deploy it in that network or give it a route.

Just write a darn playbook for this, already, and include the target!

SFS' MAD-PGAF is a tool for [SFS-SysOps](https://gitlab.com/sofreeus/sfs-sysops)
