# How To Configure The New Disk

**DRAFT** This is full of arbitrary literals for mount-point, logical volume size, mount options, and so on. It should be updated with agreed-upon final values prior to use.

---

This doc is to help the sysadmin who has just installed a fast disk in a machine and wants to configure it in a separate VG so that IO intensive work may be directed to it.

If this doc is written correctly and followed carefully, a team of several sysadmins with identical hardware will be able to produce identical configurations, even without Bash scripting or Ansible automation, so that systems which have names that only differ on index may be expected to have identical configuration.

Find the disk:

```bash
lsblk -d
```

Search the output for a device like sdb, sdc, sdd, or other, where the size approximately matches the size of the disk you installed.

For example:

In the below output, I am looking for a new disk of 512GiB, so my disk is sdb.

```
dlwillson@bear-metal:~$ lsblk -d
NAME  MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
loop0   7:0    0  49.8M  1 loop /snap/snapd/18357
loop1   7:1    0  63.3M  1 loop /snap/core20/1828
sda     8:0    0    16G  0 disk 
sdb     8:16   0   512G  0 disk 
sr0    11:0    1     4M  0 rom  
dlwillson@bear-metal:~$ 
```

Pre-pend `/dev/` to create the device name. Test that you have determined the correct device name, like this:

```bash
export DISK=sdb
export DEVICE=/dev/$DISK
lsblk -d $DEVICE
```

For example:

```
dlwillson@bear-metal:~$ export DISK=sdb
dlwillson@bear-metal:~$ export DEVICE=/dev/$DISK
dlwillson@bear-metal:~$ lsblk -d $DEVICE
NAME MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sdb    8:16   0  512G  0 disk 
dlwillson@bear-metal:~$ 
```

Remove all partitions and partition the device with one big partition for LVM. This is not strictly necessary, but it creates good evidence for anyone attempting to discover the system config later.

```bash
sudo parted $DEVICE mklabel msdos
sudo parted $DEVICE mkpart primary 0% 100%
# make it an lvm partition
sudo parted $DEVICE set 1 lvm on
```

Run `lsblk` to see the partition. The partition's device name will be the disk's device name with '1' appended. So, if the disk is /dev/sdc, the partition will be /dev/sdc1.


```bash
export PART=${DEVICE}1
echo $PART

# Create an LVM filesystem on the partition using `pvcreate`
sudo pvcreate $PART
# NOTE: The device-name of the physical volume is the same as the device-name of the partition

# Create a volume group named vg-fast and assign the physical volume to it
sudo vgcreate vg-fast $PART

# Create a logical volume named lv-fast on vg-fast using half the available space
# If it seems appropriate, use all the available space by changing this to 100%FREE
# But in that case, why use LVM?
sudo lvcreate vg-fast -n lv-fast -L 50%FREE

# Confirm
sudo lvdisplay
# Look for a line something like: LV Path ... /dev/vg-fast/lv-fast

# Create a filesystem on the logical volume
sudo mkfs.ext4 /dev/vg-fast/lv-fast

# Create a mount-point directory
sudo mkdir -p /local/fast
# Always make mount-points immutable
sudo chattr +i /local/fast

# Register lv-fast to mount to /local/fast
echo "/dev/vg-fast/lv-fast /local/fast ext4 defaults 1 2" | sudo tee -a /etc/fstab
# Confirm
tail /etc/fstab

# Mount the logical volume to the directory
mount -av
```

You want to see something like this:

```
dlwillson@bear-metal:~$ sudo mount -av 
...
/local/fast              : successfully mounted
dlwillson@bear-metal:~$ 
```

If you do, give the machine a reboot, and a full QA test.
