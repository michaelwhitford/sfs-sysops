# SFS SysOps

This is where all the things around SFS' IT systems go.

******

Ansible info available in the Ansible folder. Jumphost info is in ssh.config.

Monitoring, Alerting, and Dashboards with Prometheus, Grafana, and friends:
- [Ours](mad-pgaf/)
- [The Class](https://gitlab.com/sofreeus/mad-pgaf/)
