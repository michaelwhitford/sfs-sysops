# How To Make A Machine for SFS
with Proxmox:
- Clone one of the template machines
- extend disk
with cloud-init:
- Set key or password
- Set ip static or dhcp
with Ansible:
- Add admins
