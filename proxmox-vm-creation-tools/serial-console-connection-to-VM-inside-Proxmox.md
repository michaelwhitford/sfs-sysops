### Serial / terminal console connection to a VM guest from the PVE host
This will allow you to paste inside of the browser to the terminal.
It will also allow you to connect directly to the VM from PVE's terminal. 
`qm terminal <vmid#>` and that's like SSHing into the machine. You're sitting right on the box.
If that doesnt float your boat, the console `xterm.js` will now work, and you can paste into the terminal from your web browser.
[This post from 2016 is what demonstrates how to do it.](https://ubuntuforums.org/showthread.php?t=2343595) 

#### Adding a serial connection to your VM inside of Proxmox
First, add a serial connection to the machine and attach it to the host socket:
`qm set <vmid#> --serial0 socket`

Once that is ready, boot the VM you're modifing. 

Create a file: `sudo touch /lib/systemd/system/ttyS0.service`
set it's permissions to 644: `sudo chmod 644 /lib/systemd/system/ttyS0.service`
Now let's add the content that's required to make this work, this is what should be in the file:
```
[Unit]
Description=Serial Console Service

[Service]
ExecStart=/sbin/getty -L 115200 ttyS0 vt102
Restart=always

[Install]
WantedBy=multi-user.target
```

Last step,
After creating, saving, and editing the permissions, 
You still need to reloaded systemctl, enabled that service, and then start it:

`sudo systemctl daemon-reload`
`sudo systemctl enable ttyS0`
`sudo systemctl start ttyS0`