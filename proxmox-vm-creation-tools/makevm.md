docs for the makevm script

a copy of makevm is installed in proxmox:/usr/local/bin/

1. choose machine/service name
2. upload the public key /tmp/$MACHINE_NAME.pub where $MACHINE_NAME is actual vm hostname
3. login to proxmox as sudoer
4. run command: sudo makevm $MACHINE_NAME
5. login to $MACHINE_NAME using your matching SSH private key as $MACHINE_NAME-admin



999. end of file
