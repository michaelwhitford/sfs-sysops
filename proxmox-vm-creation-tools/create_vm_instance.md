# How to convert a VM Template into a webserver host
This process involves preparing virtual disks, cloning a pre-established template, and changing some configurations.

---
Set variables for your template's VM id, and the ID your new machine will be.

---
## Making Virtual Disks and the new machine
1. Type the following command:
```
sudo pvesm alloc local-lvm $vmid vm-$vmid-(machine_name)-1 (storageinGB)G
```
You can repeat this command but increase the number 1 to make more drives

2. Type the following commands:
```
sudo qm clone $template_vmid $vmid --name (name)

sudo qm resize $vmid scsi0 32G

sudo qm set $vmid --scsi1 local-lvm:vm-$vmid-(name)-1,size=256G
```

Repeat the last command increasing the number 1 until you have set all the drives you created

3. Type this command to give the virtual machine an SSH Key
```
sudo qm set $vmid --sshkey ~/.ssh/id_rsa.pub
```
Note, your machine needs an SSH Key before you run this

4. In the proxmox GUI, set the VM's ip to DHCP
![image](ips.png)

#### This creates the host VM for a webserver, congratulations!