# User Management for Proxmox

1. Ensure that you are a Proxmox Cluster Admin. Log in at proxmox.sofree.us. Your username is whatever it is at GitLab.com. The realm is 'Proxmox VE...' not 'Linux PAM...'.
2. Ensure that the user you are adding or enabling is highly trusted. If you're not sure, they aren't highly trusted.
3. Ensure that the user has a good phone number in the comment field.
4. Ensure that the user is a member of the correct groups. At the time of this writing, there's just "Cluster Administrators". We should probably find/create some more middle-admin roles.
5. Username should match their username on GitLab.com

Always send secrets by [One Time Secret](www.onetimesecret.com) (preferred) or Mattermost DM or SMS message(non-preferred). Never send secrets by email. Do not send username or login address with the secret. i.e. send the secret bare with no description or supporting information. Send the other information with a link to OTS or a description of where to expect the secret.

Like this:
hostname: proxmox.sofree.us
username: $YOUR_GITLABDOTCOM_USERNAME
password: https://onetimesecret.com/secret/6hc...n8k
