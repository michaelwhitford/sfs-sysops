# Updating the kernel in proxmox 

Results in breaking LVM, and grub cannot find the right UUID and boot.

Please refer to the following post for reference: 
https://forum.proxmox.com/threads/error-disk-lvmid-not-found-grub-rescue.123512/

## Fixing the GRUB uuid

You will need to have a USB stick to boot from.

Then run `lvs` to display you have everything up.

Then create a volume group, and then it should re-attach the UUID to grub when making the new logical volume group. 

`lvcreate pve --size 1G --name bob`



