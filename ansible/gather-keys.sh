#!/bin/bash -eu
echo A couple ways to gather host keys:

echo "ansible-playbook "$@" --list-hosts | grep '^      ' | /update-host-key.sh"
echo "ansible -i inventory all --list -i inventory | grep -v hosts\( | ./update-host-key.sh"
