# ansible collections

[using ansible collections](https://docs.ansible.com/ansible/latest/collections_guide/index.html)

## jmespath

python module required for json jinja filter

## proxmoxer

python module required for community.general.proxmox_kvm

## community.general.proxmox_kvm

the primary ansible collection for interacting with the proxmox api
