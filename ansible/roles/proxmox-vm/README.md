# proxmox-vm role

Create a vm in proxmox.

Uses host_vars to create/configure the vm.

### Examples

```
#host_vars/k0s-c1.int.sofree.us

vm:
  name: k0s-c1
  memory: 4096
  sockets: 1
  cores: 4
  # host cpu required for el 9+
  cpu: host
  ostype: l26
  clone: 9010
  full: true
  storage: "local-lvm"
  node: proxmox
  description: "k0s-c1 initial controller"
  scsi:
    # add 40GB disk, rocky cloud image uses scsi0 with 10G base
    scsi1: "local-lvm:40,format=raw"
```

# host_vars/elvira.int.sofree.us
vm:
  name: elvira
  memory: 4096
  sockets: 1
  cores: 4
  cpu: host
  ostype: l26
  storage: "local-lvm"
  disk_size: 100
  node: owl
  description: "elvira wireguard vpn server"
