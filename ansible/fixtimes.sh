#!/bin/bash -eu
. ansiblerc
ssh mailman.thegeek.nu sudo /usr/sbin/ntpdate time-c-b.nist.gov
ansible fishes.sofree.us,zimbra.* -i inventory -a "/usr/sbin/ntpdate time-c-b.nist.gov" -b
