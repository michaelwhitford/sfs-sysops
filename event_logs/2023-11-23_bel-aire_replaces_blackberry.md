# Notes for Blackberry replacement by Bel-Aire

Bel-Aire is a replacement and upgrade for Blackberry.

Blackberry was a Pi 1B; Bel-Aire is a Pi 2B.

I'd like Bel-Aire to have an internal IP provided by DHCP and a static external IP. I've done this on other machines.

```bash
grep -R '67.42.246' etc/
```

blackberry...

```
$ cat etc/dhcpcd.conf 
...
# Inform the DHCP server of our hostname for DDNS.
hostname blackberry.sofree.us
...
interface eth0
static ip_address=67.42.246.117/28
static routers=67.42.246.126
static domain_name_servers=8.8.4.4 8.8.8.8
```

lists...

```
dlwillson@lists:~$ cat /etc/network/interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto ens18 ens18:0

iface ens18 inet static
  address 192.168.73.23
  netmask 255.255.255.0
  dns-nameserver 192.168.73.254

iface ens18:0 inet static
  address 67.42.246.123
  netmask 255.255.255.240
  gateway 67.42.246.126 
  dns-nameserver 8.8.8.8
```

It turns out to be hard with debian bookworm, because debian has moved to Network Manager.

/etc/network/interfaces is no longer the way.

Solved by adding two static IP addresses to the interface using nmtui. Not as satisfactory as one static and one dynamic or two reservations, but satisfactory, nonetheless.

# Copy ssh host keys

Copied /etc/ssh/ from blackberry to /etc/ssh-blackberry/ on Bel-Aire, then ran this:

```bash
for K in ssh_host_*_key*; do echo $K; sudo cp -v ../ssh-blackberry/$K .; done
sudo reboot
```

# Copy in active users' old home folders

... as ~/blackberry-crashrec

```bash
for U in *; do echo $U; sudo cp -rPv ~dlwillson/blackberry/home/$U $U/blackberry-crashrec; sudo chown --recursive $U:$U $U/blackberry-crashrec; done | tee ~/restore-homes-$(date +%s)-output.log
for U in *; do echo $U; sudo diff --recursive ~dlwillson/blackberry/home/$U $U/blackberry-crashrec; done | tee ~/restore-homes-$(date +%s)-output.log
```

# Slow attacks on ssh

```bash
sudo apt install ufw
sudo ufw limit ssh
sudo ufw enable
sudo ufw status verbose
```

Not doing anything with fail2ban at this time. Maybe later.