# Show arp results

`show-arp-results` must be run on blackberry or another machine with an SFS HQ public IP address.

It tries to arp resolve all the IP addresses in the series and reports which IPs resolve and which don't.

Since ARP is not decline-able, this result, unlike the results of ping, is authoritative as we get.
