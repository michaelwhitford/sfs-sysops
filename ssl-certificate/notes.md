# How To Update the HTTP SSL Certificate

For several years, we have purchased SFS' wildcard cert from ssl2buy.com

alpha ssl wildcard *.sofree.us

---

Upload csr.conf to happy

$ ssh happy.int.sofree.us

---

$ vim csr.conf

$ openssl genrsa -out ./sofree.us.key 2048

$ openssl req -config csr.conf -new  -key ./sofree.us.key -out ./sofree.us.csr -verbose

$ cat sofree.us.csr

https://medium.com/curiouscaloo/how-to-generate-a-wildcard-cert-csr-with-a-config-file-for-openssl-8a6613ab342f

https://apfelboymchen.net/gnu/notes/openssl%20multidomain%20with%20config%20files.html

email verification is easiest, by the next time, a distribution list of trusted admins should be set up

Get the PKCS7 not the X509 because it has the whole chain

https://support.aerofs.com/hc/en-us/articles/205007260-How-Do-I-Convert-My-SSL-Certificate-File-To-PEM-Format-

```bash
# Convert PKCS7 / P7B to PEM
openssl pkcs7 -print_certs -in STAR.sofree.us.p7b -out STAR.sofree.us.pem

# Append the unencrypted private key to the cerficate chain
cat sofree.us.key  >> STAR.sofree.us.pem

# Vault that fucker
ansible-vault encrypt STAR.sofree.us.pem --output STAR.sofree.us.pem.ansible_vault
```

Update the sfs-sysops repo with the new vaulted certificate right naow!

## Update happy.sofree.us

```bash
# Upload the pem file to happy
scp STAR.sofree.us.pem happy.sofree.us:
```

Then, interactive on happy, do this:

```bash
# move cert into place
cat STAR.sofree.us.pem | sudo tee /etc/ssl/certs/STAR.sofree.us.pem
rm STAR.sofree.us.pem

# restart haproxy
sudo systemctl restart haproxy
```

Create a playbook to do that ^


## Update Zimbra

1. Verify recent backups
2. Update everything
3. Power off
4. Take a snapshot
5. Power on

```bash
# Upload the pem file
scp STAR.sofree.us.pem blue.int.sofree.us:
# Log in
ssh blue.int.sofree.us
# Put the pem file where the 'zimbra' user can reach it
sudo mv STAR.sofree.us.pem ~zimbra/ssl/zimbra/commercial/
sudo chown zimbra. ~zimbra/ssl/zimbra/commercial/STAR.sofree.us.pem
# Become the 'zimbra' user
sudo su - zimbra
cd ~/ssl/zimbra/commercial/
# Back up the old cert files
mkdir -p backup
cp commercial* backup/
```

Split the pem into these three files:
- commercial.crt
- commercial_ca.crt
- commercial.key

One way to create the above files is to copy the pem over all three files and then edit them into shape.

```bash
for F in commercial*; do cat STAR.sofree.us.pem > $F; done

vim commercial.crt
# delete everything after the first `-----END CERTIFICATE-----`

vim commercial_ca.crt
# delete the first `-----END CERTIFICATE-----` and everything before it
# delete `-----BEGIN RSA PRIVATE KEY-----` and everything after it

vim commercial.key
# delete everything before `-----BEGIN RSA PRIVATE KEY-----`
```

```bash
# Verify the new certificate with the private key
/opt/zimbra/bin/zmcertmgr verifycrt comm commercial.key commercial.crt commercial_ca.crt
# Deploy the new certificate
# NOTE: The private key is not mentioned
/opt/zimbra/bin/zmcertmgr deploycrt comm commercial.crt commercial_ca.crt
zmcontrol stop && zmcontrol stop && zmcontrol start
```

Browse the site and verify the new expiration date on the certificate.


### Update GitLab pages

Browse to https://gitlab.com/sofreeus/newsfs.gitlab.io/pages

1. Turn off force HTTPS
2. Delete the old certificates
3. Create the new certificates
4. Turn on force HTTPS
