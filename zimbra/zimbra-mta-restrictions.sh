zmprov mcf \
	zimbraMtaRestriction reject_unknown_client \
	zimbraMtaRestriction reject_non_fqdn_sender \
	zimbraMtaRestriction reject_unknown_hostname \
	zimbraMtaRestriction reject_unknown_sender_domain \
	zimbraMtaRestriction "reject_rbl_client dnsbl-1.uceprotect.net"
# actually, I used the admin GUI, because this didn't do it all.
# TODO: backport this from zmprov
