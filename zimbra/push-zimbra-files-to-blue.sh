#!/bin/bash -eu
[[ -f zimbra-config ]] || ansible-vault decrypt --ask --output=zimbra-config zimbra-config.ansiblevault
scp -pr zimbra-* blue.int.sofree.us:/tmp/
