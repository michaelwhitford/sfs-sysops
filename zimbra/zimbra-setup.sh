#!/bin/bash -eu

# change the ssh port
echo "Remember to change the ssh port"
# TODO: automate this, instead

# update the firewall
# http is on 8080, try opening that to haproxy and https://webmail.sofree.us
sudo firewall-cmd --zone=public --add-port=8443/tcp --permanent
sudo firewall-cmd --zone=public --add-port=25/tcp --permanent
sudo firewall-cmd --zone=public --add-port=587/tcp --permanent
sudo firewall-cmd --zone=public --add-port=7993/tcp --permanent

# update the system
sudo yum -y upgrade

# add perl (required)
sudo yum -y install perl net-tools

# add libreoffice for high fidelity open document preview
# see: https://zimbra.github.io/installguides/latest/single.html
sudo yum install -y libreoffice libreoffice-headless libreoffice-langpack-{en,es}

# remove postfix to avoid a port conflict
sudo yum -y remove postfix

# add host record (required)
grep blue.sofree.us /etc/hosts || echo '67.42.246.114 blue.sofree.us blue' | sudo tee -a /etc/hosts

# download and unpack Zimbra Collaboration Suite
if ! [[ -f zcs-8.8.11_GA_3737.RHEL7_64.20181207111719.tgz ]]
then
	curl -O https://files.zimbra.com/downloads/8.8.11_GA/zcs-8.8.11_GA_3737.RHEL7_64.20181207111719.tgz
	curl -O https://files.zimbra.com/downloads/8.8.11_GA/zcs-8.8.11_GA_3737.RHEL7_64.20181207111719.tgz.md5
	# TODO: verify a good download by comparing the md5sum of the tgz with the content of the md5
fi
if ! [[ -d zcs-8.8.11_GA_3737.RHEL7_64.20181207111719 ]]
then
	tar xvf zcs-8.8.11_GA_3737.RHEL7_64.20181207111719.tgz
fi

# setup redirector
sudo yum -y install httpd
cat zimbra-redirect.html | sudo tee /var/www/html/index.html
sudo systemctl enable httpd
sudo systemctl restart httpd
sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
sudo firewall-cmd --reload

# echo some possibilities, none of which are perfectly satisfactory
echo "Now do this:
sudo su -
cd $PWD/zcs-8.8.11_GA_3737.RHEL7_64.20181207111719/
./install.sh --uninstall
./install.sh ../zimbra-config"
