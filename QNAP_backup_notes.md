## QNAP NAS:

### Location: Garheads DC

## Notes about backup pull

- ProxMox backups located on NEO (both at SFS Headquarters)
- QNAP nfs mount
  - nfs mount on debian 11 server.
- Bash script on deb server
  1. copy previous output to running log file
  2. clear output log
  3. rsync from neo to debian nfs mount.
  4. curl via webhook to annouce completion in sysops on MM.

```
#!/bin/bash -eu

rundate=$(date)

cat /tmp/backup.log /tmp/runningbackup.log

echo "----------$rundate----------" > /tmp/backup.log
echo "Begin Backup" >> /tmp/backup.log

rsync -arzPh neo.int.sofree.us:/mnt/backups/ /backups &>> /tmp/backup.log

echo >> /tmp/backup.log
echo >> /tmp/backup.log
findate=$(now)
echo "----------$findate----------"

mmOut=$( cat /tmp/backup.log )

curl -X POST -H 'content-Type: application/json' -d '{"text": "QNAP Backup Complete"}' https://mattermost.sofree.us/hooks/(webhook goes here)
```