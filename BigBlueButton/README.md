# BigBlueButton

This directory contains notes and documentation related to supporting SFS's 
use of Big Blue Button for video meetings and support of remote learning.

## Deployment

- SFS instance url: bbb.sofree.us
- Product: https://github.com/bigbluebutton
- Container config: https://github.com/bigbluebutton/docker

The current environment runs on the host rcvsrv1 @ 64.40.21.212, but it the deployment is running out of the wrong server, so it might be prudent to redeploy. We can use rcvsrv2 to both test out the new cert challenge method and to redeploy in correct pre-defined location.

There were some issues during deployment on the version that was available during nov 2022 [v2.5.2](https://github.com/bigbluebutton/docker/commit/2ff3b8c4e20f541fe3cb4331aa4f98bff38b501c), which may already be resolved - Some details below. 

---

## Issues

### Logs

- Logs for the greenlight container, which is our entrypoint are on the `greenlight_1` container at `/log/production.log`

*I gave chatGPT the above as a promot Below is what it returned: -- @cfedde*

If you are trying to access the logs for the Greenlight container in the Big Blue Button application, you can use the following command to access the production log:

```bash
docker exec greenlight_1 cat /log/production.log
```

This command will display the contents of the production.log file located in the /log directory of the greenlight_1 container.

You can also use the tail command to view the latest entries in the log file:

```bash
docker exec greenlight_1 tail -f /log/production.log
```

This will show you the last 10 entries in the log file, and it will automatically update as new entries are added to the log. You can press "Ctrl + C" to exit the tail command when you are done viewing the log.


### Ports

- The port generated by the setup script assigned the wrong ports to the greenlight container, I changed these to port 80. Greenlight should be accessible from http://yourchosendomainname:port/b


### Build

- The setup script generates a compose file based on docker-compose.tmp.yml. I edited the environment entry `MS_RTP_LISTEN_IP` for the `webrtc-sfu` service (container). After edit it reads `MS_RTP_LISTEN_IP: '{"ip":"0.0.0.0", "announcedIp":"${EXTERNAL_IPv4}"}'`

- This [issue 236](https://github.com/bigbluebutton/docker/issues/236) details a problem during build of the `bbb-web` image caused by jcenter being discontinued. Per the issue I had to make modificiations to the `./mod/bbb-web-/Dockerfile` just before the `#Compile bbb-web` section comment

    ```bash
    RUN sed -i 's/jcenter/mavenCentral/g' /bbb-web/build.gradle && sed -i 's/jcenter/mavenCentral/g' /bbb-web/pres-checker/build.gradle
    ```

- I also updated the version of mediasoup being used by modifying `./mod/webrtc-sfu/Dockerfile` just after the `ENV NODE_ENV production`

    ```bash
    RUN sed -i 's/3.9.10/3.10.12/g' /app/package.json && sed -i 's/3.6.43/3.6.58/g' /app/package.json
    ```

### Invalid Endpoint and Secret

- This error shows up when opening the greenlight dashboard, I had to add the FQDN used for access to the environment on the docker host's `/etc/hosts` file. Point the FQDN to the gateway of the docker network the stack is deployed to. By default it seemed to be `10.7.7.1`

### Audio Failure - ICE Error 1004

- After deployment I stopped the stack with `docker-compose stop` and then edited `./mod/freeswitch/conf/sip_profiles/external.xml`. Changed the entry 

    Before
    ```bash
    param name="rtp-ip" value="$${external_ip_v4}"
    ```

    After
    ```bash
    param name="rtp-ip" value="$${local_ip_v4}"
    ```

---

## SSL Certs

There was a recent event that took out BBB for SFS.  A main feature of this
were security configuraions on firewalls in front of BBB.

### How did we fix it?

We had tried to identify the specific addresses that the verification comes from during initial setup but it was impossible to get a discrete list. We momentarily disabled the country blocker and ran docker-compose restart on the compose, which regenerated the certificate successfully and then we re-enabled the country blocker.

### How do we make this better?

- We could attempt to find the specific addresses or countries that letsencrypt uses for this verification
- We could attempt to swap the default verification mechanism to a different one like dns, which would bypass the requirement to be validated from multiple remote hosts and instead require some magic texts in dns.

### What's next?

This will happen again when the cert expires so a decision should be made on a permanent fix if we'd to continue using the deployment and avoid the issue in the future. The current cert's validity period.

Issued On	Friday, February 3, 2023 at 8:35:58 AM
Expires On	Thursday, May 4, 2023 at 9:35:57 AM

### Next Steps

It looks like there is no list of addresses where an HTTP-01 challenge will come from:

- https://letsencrypt.org/docs/faq/#what-ip-addresses-does-let-s-encrypt-use-to-validate-my-web-server
- https://letsencrypt.org/2020/02/19/multi-perspective-validation.html

They view this as a feature.  Thus the port 80 config on the web server must be open to the whole internet, configured only to respond to the challenge and be configured to defend itself from abuse.

The alternative here is to do the DNS-01 challenge approach:
Looking at https://letsencrypt.org/docs/ and reviewing https://letsencrypt.org/docs/challenge-types/ it looks like DNS-01 challenge type requires code work to be done and depends on an api or other means to automate updating a DNS record.

## Proposal.

1. Explore and POC the DNS-01 challenge approach.  Perhaps on some sandbox web server.
2. Build the automation and monitoring needed to maintain this.
3. Put it into production.
