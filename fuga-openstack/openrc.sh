#!/usr/bin/env bash

# To use an OpenStack cloud you need to authenticate against the Identity
# service named keystone, which returns a **Token** and **Service Catalog**.
# The catalog contains the endpoints for all services the user/tenant has
# access to - such as Compute, Image Service, Identity, Object Storage, Block
# Storage, and Networking (code-named nova, glance, keystone, swift,
# cinder, and neutron).
export OS_AUTH_URL="https://core.fuga.cloud:5000/v3"

# With the addition of Keystone we have standardized on the term **project**
# as the entity that owns the resources.
export OS_PROJECT_ID="19cbfb97881a4658aa0059cf0106dc06"
export OS_PROJECT_DOMAIN_ID="88637c7166d94c92ad08bd684a2a3642"
if [ -z "$OS_PROJECT_DOMAIN_ID" ]; then unset OS_PROJECT_DOMAIN_ID; fi
export OS_USER_DOMAIN_ID="88637c7166d94c92ad08bd684a2a3642"
if [ -z "$OS_USER_DOMAIN_ID" ]; then unset OS_USER_DOMAIN_ID; fi


# unset v2.0 items in case set
unset OS_TENANT_ID
unset OS_TENANT_NAME

# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_USER_ID="d66300abc9314f168a1d5505094445a0"

# With Keystone you pass the keystone password.
echo "Enter password for TEAM Credential with ID $OS_USER_ID: "
read -sr OS_PASSWORD_INPUT
export OS_PASSWORD=$OS_PASSWORD_INPUT

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME="ams2"
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi

export OS_INTERFACE="public"
export OS_IDENTITY_API_VERSION=3