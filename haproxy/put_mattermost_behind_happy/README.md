Get IP address of internal machine.

![screen1.png](screen1.png)

Add host.int A record to Gandi, pointing at internal IP address

![screen2.png](screen2.png)

Add host CNAME record to Gandi, pointing at happy.sofree.us

![screen3.png](screen3.png)

Add rules and backend to haproxy config

```bash
vim haproxy.cfg
```

```
...
     acl fishes hdr(host) -i fishes.sofree.us
+    acl mattermost hdr(host) -i mattermost.sofree.us
...
     use_backend fishes if fishes
+    use_backend mattermost if mattermost
...
+backend mattermost
+    server  mattermost mattermost.int.sofree.us:8065 check
```

Upload haproxy.cfg.

```bash
scp haproxy.cfg happy.int.sofree.us:
ssh happy.int.sofree.us
```

Check it. Update haproxy.cfg. Restart haproxy.

```bash
sudo haproxy -f haproxy.cfg -c
cat haproxy.cfg | sudo tee /etc/haproxy/haproxy.cfg
sudo systemctl restart haproxy.service
```

Test.

```bash
dig cname mattermost.sofree.us
curl -v mattermost.sofree.us
curl https://mattermost.sofree.us
xdg-open https://mattermost.sofree.us
```

Next steps:

Configure Mattermost.

Update Ansible inventory and add admins.

Add node_exporter and configure Prometheus to watch it.
